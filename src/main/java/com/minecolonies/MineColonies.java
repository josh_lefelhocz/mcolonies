package com.minecolonies;

import net.minecraftforge.common.MinecraftForge;

import com.minecolonies.blocks.ModBlocks;
import com.minecolonies.configuration.ConfigurationHandler;
import com.minecolonies.event.EventHandler;
import com.minecolonies.items.ModItems;
import com.minecolonies.items.crafting.RecipeHandler;
import com.minecolonies.lib.Constants;
import com.minecolonies.network.GuiHandler;
import com.minecolonies.network.PacketHandler;
import com.minecolonies.proxy.IProxy;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.SidedProxy;

@Mod(modid = Constants.MODID, name = Constants.MODNAME, version = Constants.VERSION)
public class MineColonies
{
	
	// Wait, from where comes the org.apache.logging.log4j.Logger;?
	// What must I do to get this class?
	//public static Logger logger;
	
	public static final PacketHandler packetHandler = new PacketHandler();
	
	@Mod.Instance
	public static MineColonies instance;
	
	@SidedProxy(clientSide = Constants.CLIENTPROXYLOCATION, serverSide = Constants.COMMONPROXYLOCATION)
    public static IProxy proxy;
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		//logger = event.getModLog();
		
		ConfigurationHandler.init(event.getSuggestedConfigurationFile());
		
		ModBlocks.init();
		
		ModItems.init();
		
		proxy.registerKeybindings();//Schematica
	}
	
	@Mod.EventHandler
	public void init(FMLInitializationEvent event)
	{
		//packetHandler.initialize(); needed?
		
		NetworkRegistry.instance().registerGuiHandler(instance, new GuiHandler());
		
		proxy.registerTileEntities();
		
		RecipeHandler.init();
		
		MinecraftForge.EVENT_BUS.register(new EventHandler());
		
		proxy.registerEvents();//Schematica
		
		proxy.registerEntities();
		
		proxy.registerEntityRendering();
	}
	
	@Mod.EventHandler
	public void postInit(FMLInitializationEvent event)
	{
		//packetHandler.postInitialize(); needed?
	}
}