package com.minecolonies.network;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import com.minecolonies.client.gui.GuiTownHall;
import com.minecolonies.client.gui.GuiTypable;
import com.minecolonies.tilentities.TileEntityTownHall;

import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler
{
	
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case 0:
				// TODO
				break;
		}
		return null;
	}
	
    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
    {
        switch(ID)
        {
            case 0:
                return new GuiTownHall((TileEntityTownHall) world.getBlockTileEntity(x, y, z), player, world, x, y, z);
            case 1:
                return new GuiTypable((TileEntityTownHall) world.getBlockTileEntity(x, y, z), player, world, x, y, z);
        }
        return null;
    }
}
