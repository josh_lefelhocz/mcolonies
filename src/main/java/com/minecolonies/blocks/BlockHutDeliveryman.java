package com.minecolonies.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockHutDeliveryman extends BlockInformator
{
	
    public final String name = "blockHutDeliveryman";

    protected BlockHutDeliveryman(int blockID)
    {
        super(blockID, Material.wood);
        setUnlocalizedName(getUnlocalizedName());
        GameRegistry.registerBlock(this, getUnlocalizedName());
    }

    @Override
    public String getName()
    {
        return name;
    }

	@Override
	public TileEntity createNewTileEntity(World world) 
	{
		// TODO
		return null;
	}
}