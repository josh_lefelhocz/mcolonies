package com.minecolonies.blocks;

import net.minecraft.block.Block;

public class ModBlocks
{
	
    public static Block blockHutTownhall,
    blockHutMiner,
    blockHutLumberjack,
    blockHutBaker,
    blockHutBuilder,
    blockHutDeliveryman,
    blockHutBlacksmith,
    blockHutStonemason,
    blockHutFarmer;

    public static void init()
    {
    	/**
    	 *  TODO: make the ids editable in the config; 
    	 *  adding the ids from the old 1.6.4 version of this mod
    	 */
    	blockHutTownhall = new BlockHutTownHall(603);
    	blockHutMiner = new BlockHutMiner(604);
    	blockHutLumberjack = new BlockHutLumberjack(605);
    	blockHutBaker = new BlockHutBaker(606);
    	blockHutBuilder = new BlockHutBuilder(611);
    	blockHutDeliveryman = new BlockHutDeliveryman(607);
    	blockHutBlacksmith = new BlockHutBlacksmith(608);
    	blockHutStonemason = new BlockHutStonemason(609);
    	blockHutFarmer = new BlockHutFarmer(610);
    }
}
