package com.minecolonies.blocks;

import java.util.ArrayList;
import java.util.UUID;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

import com.minecolonies.MineColonies;
import com.minecolonies.lib.Constants;
import com.minecolonies.tilentities.TileEntityBuildable;
import com.minecolonies.tilentities.TileEntityTownHall;
import com.minecolonies.util.CreativeTab;
import com.minecolonies.util.IColony;
import com.minecolonies.util.Utils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class BlockInformator extends Block implements IColony, ITileEntityProvider
{

	protected int workingRange;
	
	@SideOnly(Side.CLIENT)
    private Icon[] icons = new Icon[6];// 0 = top, 1 = bottom, 2-5 = sides;
	
	public BlockInformator(int blockID, Material material)
	{
		super(blockID, material);
		setCreativeTab(CreativeTab.mineColoniesTab);
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
	{
		player.openGui(MineColonies.instance, 0, world, x, y, z);
		return true;
	}
	
    @Override
    public void registerIcons(IconRegister iconRegister)
    {
        icons[0] = iconRegister.registerIcon(Constants.MODID.toLowerCase() + ":" + getName() + "top");
        icons[1] = icons [0];
        for(int i = 2; i <= 5; i++)
        {
            icons[i] = iconRegister.registerIcon(Constants.MODID.toLowerCase() + ":" + "sideChest");
        }
    }
    
    @Override
    public Icon getIcon(int side, int meta)
    {
    	return icons[side];
    }
    
    /**
     * Attempts to add citizen to a working hut
     * @param tileEntityTownHall TileEntityTownHall bound to
     * @param world world
     * @param x xcoord
     * @param y ycoord
     * @param z zcoord
     */
    public void attemptToAddIdleCitizens(TileEntityTownHall tileEntityTownHall, World world, int x, int y, int z)
    {
        TileEntity tileEntity = world.getBlockTileEntity(x,y,z);
        if(!(tileEntity instanceof TileEntityBuildable)) return;
        ArrayList<UUID> citizens = tileEntityTownHall.getCitizens();
        //TODO ATTEMPT TO ADD
    }
    
    /**
     * Sets the TE's townhall to the closest townhall
     *
     * @param world world
     * @param x xcoord
     * @param y ycoord
     * @param z zcoord
     */
    public void addClosestTownhall(World world, int x, int y, int z)
    {
        TileEntityTownHall tileEntityTownHall = Utils.getClosestTownHall(world, x, y, z);
        if(tileEntityTownHall != null)
        {
            if(world.getBlockTileEntity(x,y,z) instanceof TileEntityBuildable)
            {
                TileEntityBuildable tileEntityBuildable = (TileEntityBuildable) world.getBlockTileEntity(x,y,z);
                tileEntityBuildable.setTownHall(tileEntityTownHall); //TODO, check for owner first
                attemptToAddIdleCitizens(tileEntityTownHall, world, x, y, z);
            }
        }
    }
    
    @Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entityLivingBase, ItemStack itemStack)
    {
        if (world.isRemote) return;

        if(Utils.getDistanceToClosestTownHall(world, x, y, z) < Constants.MAXDISTANCETOTOWNHALL)
        {
            addClosestTownhall(world, x, y, z);
        }
    }
}
