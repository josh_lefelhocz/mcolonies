package com.minecolonies.util;

import net.minecraft.block.Block;
import net.minecraft.world.World;

import com.minecolonies.tilentities.TileEntityTownHall;

public class Utils
{
	
	/**
	 * Method to find the closest townhall
	 * @param world world obj
	 * @param x xCoord to check from
	 * @param y yCoord to check from
	 * @param z zCoord to check from
	 * @return closest TileEntityTownHall
	 */
	public static TileEntityTownHall getClosestTownHall(World world, int x, int y, int z)
	{
		double closestDist = 9999;
		TileEntityTownHall closestTownHall = null;
		
		if(world == null || world.loadedTileEntityList == null) 
			return null;
		
		for(Object o : world.loadedTileEntityList)
			if(o instanceof TileEntityTownHall)
			{
				TileEntityTownHall townHall = (TileEntityTownHall) o;
				
				if(closestDist > Math.sqrt(Math.sqrt((x - townHall.xCoord) * (x - townHall.xCoord) + (y - townHall.yCoord) * (y - townHall.yCoord) + (z - townHall.zCoord) * (z - townHall.zCoord))))
				{
					closestTownHall = townHall;
                    closestDist = Math.sqrt((x - townHall.xCoord) * (x - townHall.xCoord) + (y - townHall.yCoord) * (y - townHall.yCoord) + (z - townHall.zCoord) * (z - townHall.zCoord));
				}
			}
		return closestTownHall;
	}
	
	public static double getDistanceToClosestTownHall(World world, int x, int y, int z)
	{
        double closestDist = 9999;
        TileEntityTownHall closestTownHall = null;

        if(world == null || world.loadedTileEntityList == null) 
        	return -1;

        for(Object o : world.loadedTileEntityList)
            if(o instanceof TileEntityTownHall)
            {
                TileEntityTownHall townHall = (TileEntityTownHall) o;

                if(closestDist > Math.sqrt(Math.sqrt((x - townHall.xCoord) * (x - townHall.xCoord) + (y - townHall.yCoord) * (y - townHall.yCoord) + (z - townHall.zCoord) * (z - townHall.zCoord))))
                {
                    closestDist = Math.sqrt((x - townHall.xCoord) * (x - townHall.xCoord) + (y - townHall.yCoord) * (y - townHall.yCoord) + (z - townHall.zCoord) * (z - townHall.zCoord));
                }
            }
        return closestDist;
	}
	
    /**
     * Finds the highest block in one yCoord, but ignores leaves etc.
     * @param world world obj
     * @param x xCoord
     * @param z zCoord
     * @return yCoordinate
     */
	protected int findTopGround(World world, int x, int z)
	{
		int yHolder = 1;
		while(!world.canBlockSeeTheSky(x, yHolder, z))
		{
			yHolder++;
		}
        while(  world.getBlockId(x, yHolder, z) == 0 ||
               !world.getBlockMaterial(x, yHolder, z).isOpaque() ||
                world.getBlockId(x, yHolder, z) == Block.leaves.blockID)
         {
             yHolder--;
         }
         return yHolder;
	}
	
	/**
	 * Checks if the block is water
	 * @param block block to be checked
	 * @return true if is water.
	 */
	public static boolean isWater(int block) 
	{
		return (block == Block.waterStill.blockID || block == Block.waterMoving.blockID);
	}
	
	/**
	 * Checks if the block is water
	 * @param world world obj
	 * @param x xCoord
	 * @param y yCoord
	 * @param z zCoord
	 * @return true if is water
	 */
	public static boolean isWater(World world, int x, int y, int z)
	{
		return world.getBlockId(x, y, z) == Block.waterStill.blockID || world.getBlockId(x, y, z) == Block.waterMoving.blockID;
	}
}
