package com.minecolonies.configuration;

import java.io.File;

import net.minecraftforge.common.Configuration;

public class ConfigurationHandler 
{
	
	public static void init(File file)
	{
		Configuration config = new Configuration(file);
		
		try
		{
			/** TODO */
			config.load();
			//Configurations.workingRangeTownhall = config.get("Game Play", "Working Range Townhall: ", DEFAULT_WORKINGRANGETOWNHALL).getInt(DEFAULT_WORKINGRANGETOWNHALL);
            //Configurations.allowInfiniteSupplyChests = config.get("Game Play", "Allow infinite placing of Supply Chests: ", DEFAULT_ALLOWINFINTESUPPLYCHESTS).getBoolean(DEFAULT_ALLOWINFINTESUPPLYCHESTS);
        }
		finally
		{
			config.save();
		}
	}
}
